﻿using MedicalStore.EntitiesLayer;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RepositoryLayer
{
    public interface IMedicleRepository
    {
        List<Medicine> GetAll();
        Medicine Get(int id);
        bool Insert(Medicine medicine);
        bool Update(int id, Medicine medicine);
        bool Delete(int id);
    }
}
