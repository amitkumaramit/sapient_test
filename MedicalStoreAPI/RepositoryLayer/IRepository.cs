﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RepositoryLayer
{
    public interface IRepository<T> where T : class
    {
        T Insert(T entity);
        T Update(T entity);
        T Delete(T entity);
        Task<List<T>> GetAll();
    }
}
