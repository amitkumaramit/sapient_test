﻿using MedicalStore.EntitiesLayer;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace RepositoryLayer
{
    public class MedicileDBContext : DbContext
    {
        public MedicileDBContext(DbContextOptions options) : base(options)
        {
        }
        public DbSet<Medicine> medicinns { get; set; }
    }
}
