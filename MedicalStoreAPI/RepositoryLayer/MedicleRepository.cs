﻿using MedicalStore.EntitiesLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositoryLayer
{
   public class MedicleRepository: IMedicleRepository
    {
        private MedicileDBContext dbContext;
        public MedicleRepository(MedicileDBContext context)
        {
            dbContext = context;
        }

       
        public Medicine Get(int id)
        {
            return dbContext.medicinns.Where(s => s.Id == id).FirstOrDefault();
        }

        public List<Medicine> GetAll()
        {
            return dbContext.medicinns.ToList();
        }

        public bool Insert(Medicine medicine)
        {
            try
            {
                medicine.Created = medicine.Updated = DateTime.Now;
                dbContext.medicinns.Add(medicine);
                dbContext.medicinns.UpdateRange();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Update(int id, Medicine medicine)
        {
            try
            {
                var medi = dbContext.medicinns.Where(s => s.Id == id).FirstOrDefault();
                medi.Name = medicine.Name;
                medi.Notes = medicine.Notes;
                medi.Price = medicine.Price;
                medi.Quantity = medicine.Quantity;
                medi.ExpiryDate = medicine.ExpiryDate;
                medi.Brand = medicine.Brand;
                medi.Updated = DateTime.Now;

                dbContext.medicinns.UpdateRange();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                var medi = dbContext.medicinns.Where(s => s.Id == id).FirstOrDefault();
                dbContext.medicinns.Remove(medi);
                dbContext.medicinns.UpdateRange();
                return true;
            }
            catch
            {
                return false;
            }
        }


    }
}
