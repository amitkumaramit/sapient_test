﻿using MedicalStore.EntitiesLayer;
using Microsoft.AspNetCore.Mvc;
using RepositoryLayer;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MedicalStoreAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MedicineController : ControllerBase
    {
        private readonly IMedicleRepository _medicleRepository;

        public MedicineController(IMedicleRepository medicleRepository)
        {
            _medicleRepository = medicleRepository;
        }

        // GET: api/<MedicineController>
        [HttpGet]
        public IActionResult Get()
        {
            var result = _medicleRepository.GetAll();
            return Ok(result);
        }

        // GET api/<MedicineController>/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var result = _medicleRepository.Get(id);
            return Ok(result);
        }

        // POST api/<MedicineController>
        [HttpPost]
        public IActionResult Post([FromBody] Medicine medicine)
        {
            var result = _medicleRepository.Insert(medicine);
            return Ok(result);
        }

        // PUT api/<MedicineController>/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] Medicine medicine)
        {
            var result = _medicleRepository.Update(id,medicine);
            return Ok(result);
        }

        // DELETE api/<MedicineController>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var result = _medicleRepository.Delete(id);
            return Ok(result);
        }
    }
}
